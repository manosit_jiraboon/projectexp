import { HttpClient } from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
const URLAPI:string="http://localhost:32242/api/";
@Injectable({
  providedIn: 'root'
})
export class ApiService {

   httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };
  constructor(private api:HttpClient) { }

  private ActionPOST(url,dataItem:any) : Observable<any>{
   return this.api.post<any>(URLAPI+url,dataItem,{
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      })
    })
  }

 private ActionGet(url) :Observable<any>{
   return this.api.get<any>(URLAPI+url);
  }

  GetPOST(url,dataItem:any,functionCall:any){
    this.ActionPOST(url,dataItem).subscribe((data:{})=>{
      functionCall(data);
    });
  }

  GetURL(url,functionCall:any){
    this.ActionGet(url).subscribe((data:{})=>{
      functionCall(data);
    });
  }


}
