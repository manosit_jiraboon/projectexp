import { Component, OnInit } from '@angular/core';
import {ApiService } from '../api/api.service'
import {Router} from '@angular/router';
@Component({
  selector: 'app-empadd',
  templateUrl: './empadd.component.html',
  styleUrls: ['./empadd.component.css']
})
export class EmpaddComponent implements OnInit {
  empname:string="";
  empsurname:string="";
  empdate:string="";
  empsalary:string="";
  empemail:string="";
  constructor(private callAPI:ApiService,private rou : Router ) { }

  ngOnInit() {
  }
  Save(){
    if(this.empemail.indexOf("@")==-1){
      alert("กรุณากรอกอีเมล์ให้ถุกต้อง");
      return;
    }
    if(this.empemail.indexOf(".")==-1){
      alert("กรุณากรอกอีเมล์ให้ถุกต้อง");
      return;
    }

    this.callAPI.GetPOST("EmpAdd",{
      EmpName:this.empname,
      EmpSurName:this.empsurname,
      EmpDate:this.empdate,
      EmpEmail:this.empemail,
      EmpSalary:this.empsalary
    },(data:{})=>{
      alert("ดำเนินการเสร็จสิ้น");
      this.rou.navigate(["/"]);
    });

  }

}
