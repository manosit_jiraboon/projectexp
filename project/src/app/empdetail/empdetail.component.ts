import { Component, OnInit } from '@angular/core';
import {ApiService} from '../api/api.service'
import {ActivatedRoute,ActivationEnd,Router,Routes} from '@angular/router'
import {EmployeeItem}  from '../Model/EmployeeItem';
import { modelGroupProvider } from '@angular/forms/src/directives/ng_model_group';
@Component({
  selector: 'app-empdetail',
  templateUrl: './empdetail.component.html',
  styleUrls: ['./empdetail.component.css']
})
export class EmpdetailComponent implements OnInit {
  empData :Array<any>;
  numberIndex:number = -1;
  title:string="";
  totalRecord : number = 0;
  constructor(private callAPI:ApiService,private rpage :Router,private ractive:ActivatedRoute) { }
  nextxpage(){
   this.numberIndex++;
  }

backpage(){
  this.numberIndex= this.numberIndex-1;
  if(this.numberIndex==-1){
this.rpage.navigate(["/"]);
  }
}

  ngOnInit() {


    var getdetailID = this.ractive.snapshot.params["id"];
    this.callAPI.GetPOST("EmpList",{
      "id":getdetailID
    },(data:{})=>{
  this.empData = data as Array<any>;
for(let item of this.empData){
  this.numberIndex = this.numberIndex+1;
  if(item["id"]==getdetailID){
    break;
  }
}
if(this.numberIndex!=-1){
  this.totalRecord = this.empData.length-1;
}

    });
  }



}
