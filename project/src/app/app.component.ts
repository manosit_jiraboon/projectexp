import { Component } from '@angular/core';
import { Routes, RouterModule,ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'project';
  pageActive:string="";
  constructor(private rounterPage :Router,private rounterActive:ActivatedRoute){
  }

  GoPage(ctrlPage:string){
    this.pageActive = ctrlPage;
  this.rounterPage.navigate(["/"+ctrlPage]);
  }
}
