﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EmpAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpAddController : ControllerBase
    {
        // GET: api/EmpAdd
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/EmpAdd/5
        [HttpGet("{id}", Name = "GetEmpAdd")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/EmpAdd
        [HttpPost]
        public System.Data.DataTable Post(Model.Employee input)
        {
            Service.ServiceBase service = new Service.ServiceBase();
            return service.Save(input);
        }

        // PUT: api/EmpAdd/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
